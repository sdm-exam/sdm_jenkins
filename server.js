const express = require('express')
const cors = require('cors')

const app = express()
app.use(cors('*'))
app.use(express.json())

app.get('/', (request, response) => {
    response.send('<h1>Movie</h1><h2>Details</h2>')
})

app.listen(3000, '0.0.0.0', () => {
    console.log('server started on port 3000')
})
